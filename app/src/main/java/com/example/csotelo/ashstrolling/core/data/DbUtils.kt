package com.example.csotelo.ashstrolling.core.data

import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.csotelo.ashstrolling.AshstrollingApplication
import com.example.csotelo.ashstrolling.core.PokemonRestAPI
import com.example.csotelo.ashstrolling.core.data.daos.PokemonDao
import com.example.csotelo.ashstrolling.core.data.daos.PokemonTypeDao
import com.example.csotelo.ashstrolling.core.data.daos.PokemonTypeJoinDao
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import com.example.csotelo.ashstrolling.core.data.entities.PokemonType
import com.example.csotelo.ashstrolling.core.data.entities.PokemonTypeJoin
import com.example.csotelo.ashstrolling.core.data.json.PokemonTypeSlot
import com.example.csotelo.ashstrolling.utils.Utils.byteArrayFromUrl
import com.google.gson.Gson
import io.reactivex.Observable
import java.net.URL


class DbUtils {
    companion object {
        const val TAG: String = "DbUtils"
        fun getPokemonObservable(i: Int): Observable<Pokemon> {
            return Observable.create { emitter ->
                val pokemon = getPokemon(i)
                if (pokemon != null)
                    emitter.onNext(pokemon)
                else
                    emitter.onError(Exception("FAILED TO GET POKEMON"))
                emitter.onComplete()
            }
        }

        fun findPokemonObservable(i: Int): Observable<Pokemon> {
            return Observable.create { emitter ->
                val pokemon = findPokemon(i)
                if (pokemon != null)
                    emitter.onNext(pokemon)
                else
                    emitter.onError(Exception("FAILED TO FIND POKEMON"))
                emitter.onComplete()
            }
        }

        fun findPokemonsObservable(index: IntArray): Observable<Pokemon> {
            return Observable.create { emitter ->
                for (i in index) {
                    if (!emitter.isDisposed) {
                        val pokemon = findPokemon(i)
                        if (pokemon != null) {
                            emitter.onNext(pokemon)
                        } else
                            emitter.onError(Exception("FAILED TO FIND POKEMON"))
                    } else {
                        Log.d(TAG, "Observable isDisposed")
                    }
                }
            }
        }

        fun getAllPokemonsObservable(): Observable<Pokemon> {
            return Observable.create { emitter ->
                for (i in 1..GenerationsNumber.GenerationVI.totalPokemon) {
                    if (!emitter.isDisposed) {
                        val pokemon = getPokemon(i)
                        if (pokemon != null)
                            emitter.onNext(pokemon)
                        else
                            emitter.onError(Exception("FAILED TO GET POKEMON"))
                    } else {
                        emitter.onError(Exception("Observable isDisposed"))
                    }
                }
                emitter.onComplete()
            }
        }

        fun getPokemonsListObservable(indexes: List<Int>): Observable<Pokemon> {
            return Observable.create { emitter ->
                for (i in indexes) {
                    if (!emitter.isDisposed) {
                        val pokemon = getPokemon(i)
                        if (pokemon != null)
                            emitter.onNext(pokemon)
                        else
                            emitter.onError(Exception("FAILED TO GET POKEMON"))
                    } else {
                        emitter.onError(Exception("Observable isDisposed"))
                    }
                }
                emitter.onComplete()
            }
        }

        fun getPokemon(indexes: List<Int>): List<Pokemon?> {
            return indexes.map { i -> getPokemon(i) }
        }

        fun getPokemon(index: Int): Pokemon? {
            val db = PokemonDataBase.getInstance(AshstrollingApplication.context!!)
            val pokemonDao = db.pokemonDao()
            var pokemon: Pokemon? = pokemonDao.getPokemon(index)
            return try {
                if (pokemon == null) {
                    val pok = getPokemonRest(index)
                    pokemon = Pokemon.convert(pok!!)
                    pokemon.sprite = byteArrayFromUrl(pok.spriteUrl)
                    pokemonDao.insertSafe(pokemon)
                    for (slot: PokemonTypeSlot in pok.types) {
                        slot.type.url = slot.type.url // FIXME ID IS MISSING, thats why url must be refreshed
                        val pokemonType = getPokemonType(slot.type)
                        db.pokemonTypeJoinDao().insertSafe(PokemonTypeJoin(pokemon.id, pokemonType.id))
                    }
                    Log.v("NEWPOKEMON", "Got new pokemon #${pokemon.id} -> ${pokemon.name}")
                } else {
                    Log.v("OLDPOKEMON", "Found pokemon #${pokemon.id} -> ${pokemon.name}")
                }
                pokemon
            } catch (e: Exception) {
                Log.v(TAG, e.message)
                null
            }
        }

        fun findPokemon(index: Int): Pokemon? {
            val pokemon = getPokemon(index)
            if (pokemon != null) {
                pokemon.found = true
                val db = PokemonDataBase.getInstance(AshstrollingApplication.context!!)
                val pokemonDao = db.pokemonDao()
                pokemonDao.update(pokemon)
            }
            return pokemon
        }

        fun findPokemon(indexes: List<Int>): List<Pokemon?> {
            return indexes.map { i -> findPokemon(i) }
        }

        fun getPokemonType(type: com.example.csotelo.ashstrolling.core.data.json.PokemonType): PokemonType {
            val db = PokemonDataBase.getInstance(AshstrollingApplication.context!!)
            val pokemonTypeDao = db.pokemonTypeDao()
            var pokemonType: PokemonType? = pokemonTypeDao.getType(type.id)
            if (pokemonType == null) {
                pokemonType = PokemonType(type.id, type.name, type.url)
                pokemonTypeDao.insertSafe(pokemonType)
            }
            return pokemonType
        }

        fun getPokemonType(index: Int): PokemonType? {
            val db = PokemonDataBase.getInstance(AshstrollingApplication.context!!)
            val pokemonTypeDao = db.pokemonTypeDao()
            var pokemonType: PokemonType? = pokemonTypeDao.getType(index)
            if (pokemonType == null) {
                pokemonTypeDao.insertSafe(PokemonType.convert(getPokemonTypeRest(index)!!))
            }
            return pokemonType
        }

        fun getPokemonRest(index: Int): com.example.csotelo.ashstrolling.core.data.json.Pokemon? {
            val pokeAsString: String = URL(PokemonRestAPI.POKEMON_URL + index).readText()
            val pok = Gson().fromJson(pokeAsString, com.example.csotelo.ashstrolling.core.data.json.Pokemon::class.java)
            pok.spriteUrl = "${PokemonRestAPI.POKEMON_FRONT_DEFAULT_URL}${pok.id}.png"
            return pok
        }

        fun getPokemonTypeRest(index: Int): com.example.csotelo.ashstrolling.core.data.json.PokemonType? {
            val pokeTypeAsString: String = URL(PokemonRestAPI.POKEMON_TYPE_URL + index).readText()
            var retVal = Gson().fromJson(pokeTypeAsString, com.example.csotelo.ashstrolling.core.data.json.PokemonType::class.java)
            retVal.url = retVal.url
            return retVal
        }

    }
}

private fun PokemonTypeJoinDao.insertSafe(pokemonTypeJoin: PokemonTypeJoin) {
    try {
        insert(pokemonTypeJoin)
    } catch (e: SQLiteConstraintException) {
        Log.e(DbUtils.TAG, e.message)
    }
}

private fun PokemonTypeDao.insertSafe(pokemonType: PokemonType) {
    try {
        insert(pokemonType)
    } catch (e: SQLiteConstraintException) {
        Log.e(DbUtils.TAG, e.message)
    }
}


private fun PokemonDao.insertSafe(pokemon: Pokemon) {
    try {
        insert(pokemon)
    } catch (e: SQLiteConstraintException) {
        Log.e(DbUtils.TAG, e.message)
    }
}
