package com.example.csotelo.ashstrolling.listing


import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.csotelo.ashstrolling.R
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import com.example.csotelo.ashstrolling.utils.Utils
import com.example.csotelo.ashstrolling.utils.Utils.bitmapFromByteArray


class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val pokemonIdTextView: TextView = itemView.findViewById<View>(R.id.tv_pokemon_id) as TextView
    private val pokemonSpriteImageView: ImageView = itemView.findViewById<View>(R.id.iv_pokemon) as ImageView
    private val pokemonNameTextView: TextView = itemView.findViewById<View>(R.id.tv_pokemon) as TextView
    private val pokemonFoundView: TextView = itemView.findViewById<View>(R.id.tv_pokemon_found) as TextView

    fun bindTo(pokemon: Pokemon) {
        itemView.tag = pokemon.id
        pokemonIdTextView.text = itemView.context.getString(R.string.pokemon_id, Utils.toThreeDigitNumber(pokemon.id))
        pokemonNameTextView.text = if (pokemon.found) pokemon.name else "???"
        val mutableBitmap: Bitmap
        if (pokemon.sprite != null) {
            val b = bitmapFromByteArray(pokemon.sprite!!)
            mutableBitmap = b.copy(Bitmap.Config.ARGB_8888, true)
            if (!pokemon.found) {
                val image = Bitmap.createBitmap(mutableBitmap.width, mutableBitmap.height, Bitmap.Config.ARGB_8888)
                image.eraseColor(android.graphics.Color.DKGRAY)
                mutableBitmap.mask(image, PorterDuff.Mode.DST_IN, Color.DKGRAY)
            }
        } else {
            val b = itemView.context.resources.getDrawable(R.drawable.ic_help_outline_black_48dp).toBitmap()
            mutableBitmap = b.copy(Bitmap.Config.ARGB_8888, true)
            mutableBitmap.mask(b, PorterDuff.Mode.DST_IN, Color.DKGRAY)
        }
        pokemonSpriteImageView.setImageBitmap(mutableBitmap)
        if (pokemon.found)
            pokemonFoundView.visibility = View.VISIBLE
        else
            pokemonFoundView.visibility = View.GONE
    }

    private fun pokemonSpriteUrl(id: Int): String {
        return POKE_IMAGE_URL_PREFIX + id + POKE_IMAGE_URL_SUFFIX
    }

    fun clear() {
        itemView.invalidate()
        pokemonIdTextView.invalidate()
        pokemonNameTextView.invalidate()
        pokemonSpriteImageView.invalidate()
    }


    fun Bitmap.mask(mask: Bitmap, mode: PorterDuff.Mode, color: Int) {
        //You can change original image here and draw anything you want to be masked on it.
        val canvas = Canvas(this)
        val result = Bitmap.createBitmap(mask.width, mask.height, Bitmap.Config.ARGB_8888)
        val tempCanvas = Canvas(result)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        tempCanvas.drawBitmap(this, 0f, 0f, null)
        tempCanvas.drawBitmap(mask, 0f, 0f, paint)
        paint.xfermode = null

        //Draw result after performing masking
        canvas.drawBitmap(result, 0f, 0f, Paint())

    }

    fun Drawable.toBitmap(): Bitmap {

        if (this is BitmapDrawable) {
            return this.bitmap
        }

        val bitmap = Bitmap.createBitmap(this.intrinsicWidth, this.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        this.setBounds(0, 0, canvas.width, canvas.height)
        this.draw(canvas)

        return bitmap
    }

    companion object {
        private val POKE_IMAGE_URL_PREFIX = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
        private val POKE_IMAGE_URL_SUFFIX = ".png"
    }
}
