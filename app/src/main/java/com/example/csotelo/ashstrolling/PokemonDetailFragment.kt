package com.example.csotelo.ashstrolling

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.csotelo.ashstrolling.core.StringKeys
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import kotlinx.android.synthetic.main.fragment_pokemon_detail.*
import kotlinx.android.synthetic.main.poke_detail_detail.*


class PokemonDetailFragment : Fragment() {
    lateinit var pokemon: Pokemon

    companion object {
        fun forPokemon(pokemon: Pokemon): PokemonDetailFragment {
            val frag = PokemonDetailFragment()
            frag.pokemon = pokemon
            return frag
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pokemon_detail, container, false)
    }

    override fun onStart() {
        super.onStart()
        tv_id.text = "#${pokemon.id}"
        tv_name.text = "${pokemon.name}"
        tv_height.text = "${pokemon.height / 10} m"
        tv_weight.text = "${pokemon.weight / 10} kg"
        tv_abilities.text = "Abilities"
        tv_category.text = "Categories"
        Glide.with(this).load(String.format(StringKeys.SPRITE_ANIM_URL, pokemon.name)).into(gifView)

    }
}
