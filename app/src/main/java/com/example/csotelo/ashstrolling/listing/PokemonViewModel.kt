package com.example.csotelo.ashstrolling.listing


import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import com.example.csotelo.ashstrolling.core.data.repositories.PokemonRepository

class PokemonViewModel(application: Application) : AndroidViewModel(application) {
    private val mRepository: PokemonRepository = PokemonRepository(application)
    internal val allPokemons: LiveData<List<Pokemon>>?


    init {
        allPokemons = mRepository.allPokemons
    }

    fun insert(pokemon: Pokemon) {
        mRepository.insert(pokemon)
    }
}
