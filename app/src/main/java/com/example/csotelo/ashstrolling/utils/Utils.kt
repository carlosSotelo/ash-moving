package com.example.csotelo.ashstrolling.utils

import android.graphics.*
import java.net.HttpURLConnection
import java.util.*


object Utils {
    private val TWO_ZEROS = "00"
    private val ONE_ZERO = "0"

    fun toThreeDigitNumber(n: Int): String {
        if (n >= 1 && n <= 9)
            return TWO_ZEROS + n
        return if (n <= 99) ONE_ZERO + n else n.toString()
    }

    fun randomInt(max: Int, min: Int = 0): Int {
        val r = Random()
        return r.nextInt(max - min + 1) + min
    }

    fun randomIntList(max: Int, min: Int = 0, count: Int = 1): List<Int> {
        return (0 until count).map { randomInt(max, min) }
    }

    fun bitmapFromUrl(src: String): Bitmap? {
        val url = java.net.URL(src)
        val connection = url.openConnection() as HttpURLConnection
        connection.doInput = true
        connection.connect()
        val input = connection.inputStream
        return BitmapFactory.decodeStream(input)
    }

    fun byteArrayFromUrl(src: String): ByteArray? {
        return try {
            val url = java.net.URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val ret = connection.inputStream.readBytes()
            connection.inputStream.close()
            ret
        } catch (e: Exception) {
            null
        }
    }

    fun bitmapFromByteArray(bitmapdata: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.size)
    }
}
