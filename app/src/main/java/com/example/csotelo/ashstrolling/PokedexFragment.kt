package com.example.csotelo.ashstrolling

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearSmoothScroller
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import com.example.csotelo.ashstrolling.listing.PokemonAdapter
import com.example.csotelo.ashstrolling.listing.PokemonViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

class PokedexFragment : Fragment() {
    val TAG = PokedexFragment::class.java.name

    @BindView(R.id.rv_pokemons)
    lateinit var recyclerView: RecyclerView
    private lateinit var subscription: Disposable
    private lateinit var pokemonObs: Observable<Pokemon>
    private lateinit var smoothScroller: LinearSmoothScroller
    private lateinit var viewModel: ViewModelProviders
    private lateinit var adapter: PokemonAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_pokedex, container, false)
        ButterKnife.bind(this, rootView)


        val viewModel = ViewModelProviders.of(this).get(PokemonViewModel::class.java)
        val adapter = PokemonAdapter(context!!) { pokemon ->
            val fragManager = activity!!.supportFragmentManager
            fragManager.beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.content_frame, PokemonDetailFragment.forPokemon(pokemon)).commit()
        }
        viewModel.allPokemons?.observe(this, Observer<List<Pokemon>> { adapter.setPokemons(it!!) })

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(context, 2)

        smoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return LinearSmoothScroller.SNAP_TO_START
            }
        }
        recyclerView.adapter = adapter

        return rootView
    }

}

