package com.example.csotelo.ashstrolling

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.csotelo.ashstrolling.core.Ash
import com.example.csotelo.ashstrolling.core.data.DbUtils
import com.example.csotelo.ashstrolling.core.data.GenerationsNumber
import com.example.csotelo.ashstrolling.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_stroll.*


class StrollFragment : Fragment() {

    private var ash = Ash()

    @OnClick(R.id.btnN, R.id.btnO, R.id.btnE, R.id.btnS)
    fun navClick(btn: Button) {
        tvPath.append(btn.text)
        btnClear.visibility = View.VISIBLE

        updateTextViews()
    }

    @OnClick(R.id.btnClear)
    fun clearPath(btn: Button) {
        tvPath.setText("")
        btnClear.visibility = View.INVISIBLE
        updateTextViews()
    }

    @OnClick(R.id.btnStroll)
    fun ashStrollButtonClick(btn: Button) {
        val total = ash.catchPokemonInPath(tvPath.text.toString())
        val iteratorVal = (1f / total) * 100
        var pokemonsCaught = "Found: "
        progDialog.visibility = View.VISIBLE
        progressBar.progressValue = 0f

        var pokemonSubscriber = DbUtils.getPokemonsListObservable(Utils.randomIntList(GenerationsNumber.GenerationVII.totalPokemon, 1, total))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribeBy(
                        onNext = {
                            progressBar.progressValue += iteratorVal
                            pokemonsCaught += it.name + "; "
                        },
                        onError = { it.printStackTrace() },
                        onComplete = {
                            progDialog.visibility = View.GONE
                            Toast.makeText(this.context, pokemonsCaught, Toast.LENGTH_LONG).show()
                        }
                )

    }

    private fun updateTextViews() {
        val pokemonCaught = ash.catchPokemonInPath(tvPath.text.toString())
        tvPokemonCaught.text = "$pokemonCaught"
        var efficiency = 0f
        if (tvPath.text.isNotEmpty())
            efficiency = (pokemonCaught - 1) / tvPath.text.length.toFloat()
        tvPathEfficiency.text = "$efficiency %"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_stroll, container, false)
        ButterKnife.bind(this, rootView)

        return rootView
    }
}
