package com.example.csotelo.ashstrolling.core.data.repositories

import android.app.Application
import android.arch.lifecycle.LiveData

import com.example.csotelo.ashstrolling.core.data.PokemonDataBase
import com.example.csotelo.ashstrolling.core.data.daos.PokemonDao
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import io.reactivex.schedulers.Schedulers

class PokemonRepository internal constructor(application: Application) {

    internal var pokemonDao: PokemonDao
    internal var allPokemons: LiveData<List<Pokemon>>? = null

    init {
        val db = PokemonDataBase.getInstance(application)
        pokemonDao = db.pokemonDao()
        allPokemons = pokemonDao.getAllPokemons()
    }


    fun insert(pokemon: Pokemon) {
        Schedulers.computation().scheduleDirect({ pokemonDao.insert(pokemon) })
    }

}
