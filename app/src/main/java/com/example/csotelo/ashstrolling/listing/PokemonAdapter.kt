package com.example.csotelo.ashstrolling.listing

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.csotelo.ashstrolling.R
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon

class PokemonAdapter internal constructor(context: Context, val listener: (Pokemon) -> Unit) : RecyclerView.Adapter<PokemonViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var mPokemons: List<Pokemon>? = null // Cached copy of pokemons

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val itemView = mInflater.inflate(R.layout.item_pokemon, parent, false)
        return PokemonViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        if (mPokemons != null) {
            val current = mPokemons!![position]
            holder.bindTo(current)
            holder.itemView.setOnClickListener({
                listener(current)
            })
        } else {
            holder.clear()
        }
    }

    internal fun setPokemons(pokemons: List<Pokemon>) {
        mPokemons = pokemons
        notifyDataSetChanged()
    }

    // getItemCount() is called many times, and when it is first called,
    // mPokemons has not been updated (means initially, it's null, and we can't return null).
    override fun getItemCount(): Int {
        return if (mPokemons != null)
            mPokemons!!.size
        else
            0
    }
}