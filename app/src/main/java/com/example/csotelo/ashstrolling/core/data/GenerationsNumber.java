package com.example.csotelo.ashstrolling.core.data;

public enum GenerationsNumber {
    GenerationI(151),
    GenerationII(251),
    GenerationIII(386),
    GenerationIV(493),
    GenerationV(649),
    GenerationVI(721),
    GenerationVII(807);

    private final int totalPokemon;

    GenerationsNumber(int i) {
        totalPokemon = i;
    }

    public int getTotalPokemon() {
        return totalPokemon;
    }
}
