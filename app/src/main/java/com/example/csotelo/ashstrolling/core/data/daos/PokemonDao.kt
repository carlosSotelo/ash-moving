package com.example.csotelo.ashstrolling.core.data.daos

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import android.support.annotation.VisibleForTesting
import com.example.csotelo.ashstrolling.core.data.entities.Pokemon
import java.lang.reflect.Modifier.PRIVATE


@Dao
interface PokemonDao {

    @Query("SELECT * FROM pokemon ORDER BY id ASC")
    fun getAllPokemons(): LiveData<List<Pokemon>>?

    @Query("SELECT * FROM pokemon WHERE name like :filter ORDER BY id ASC")
    fun searchPokemons(filter: String): LiveData<List<Pokemon>>?

    @Query("SELECT * FROM pokemon WHERE found = 'true' ORDER BY id ASC")
    fun getAllPokemonsFound(): LiveData<List<Pokemon>>?

    @Query("SELECT * FROM pokemon WHERE id = :index")
    fun getPokemon(index: Int): Pokemon?

    @Query("SELECT * FROM pokemon WHERE id IN(:indexes) ORDER BY id ASC")
    fun getPokemons(indexes: IntArray): LiveData<List<Pokemon>>?

    @VisibleForTesting(otherwise = PRIVATE)
    @Query("SELECT COUNT(*) FROM pokemon")
    fun count(): Int?

    @VisibleForTesting(otherwise = PRIVATE)
    @Query("SELECT COUNT(*) FROM pokemon WHERE found = 'true'")
    fun countFindings(): Int

    @Insert
    fun insert(vararg pokemons: Pokemon)

    @Update
    fun update(vararg pokemon: Pokemon)

    @VisibleForTesting(otherwise = PRIVATE)
    @Query("DELETE FROM pokemon")
    fun deleteAll()

}
